import numpy as np
from sklearn import linear_model

N = [20, 100, 1000]
sims = 1000
p = 15
beta0 = 3
beta = np.array([2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0])

Sigma = 0.2*np.eye(p, dtype=float) + 0.8*np.ones((p, p))

sd_e = 5


var_betahat = np.empty([p+1, 3])
j = 0

for n in N:
	
	# Calculate X with n = 20 or 100 or 1000
	X_train = np.random.multivariate_normal(np.zeros(p), Sigma, n)
	X_test = np.random.multivariate_normal(np.zeros(p), Sigma, 1000)

	#Add a column of ones in X
	X_train_one = np.hstack((np.ones((n, 1)), X_train))
	#X_test = np.hstack((np.ones((1000, 1)), X_test))

	#Calculate beta_hat
	var = np.linalg.inv(np.dot(X_train_one.T, X_train_one))*sd_e**2
	var_betahat[:, j] = np.diag(var)

	# Initialize arrays
	beta_error = np.empty([sims, p+1])
	prediction_error = np.empty([sims, 1000])
	f_error = np.empty([sims, 1000])

	# Run the experiment 1000 times
	for i in range(sims):

		Y_train = beta0 + np.dot(X_train, beta) + np.random.normal(0, sd_e, 1)
		Y_test = beta0 + np.dot(X_test, beta) + np.random.normal(0, sd_e, 1)
		E_Y_test = beta0 + np.dot(X_test, beta)

		# Create linear regression object and calculate intercept
		regr = linear_model.LinearRegression(fit_intercept=False, normalize=False)

		# Train the model using the training sets
		regr.fit(X_train, Y_train)

		# Get the coefficients and intercept term
		beta0_hat = regr.intercept_
		beta_hat = regr.coef_

		# Calclulate errors
		beta_error[i, ] = np.insert(beta, 0, beta0) - np.insert(beta_hat, 0, beta0_hat)
		prediction_error[i, ] = Y_test - regr.predict(X_test)
		f_error[i,] = E_Y_test - regr.predict(X_test)


	# Calculate bias, squared bias, variance and MSE for beta_hat
	bias = np.mean(beta_error, axis = 0)
	squared_bias = bias ** 2
	variance = np.var(beta_error, axis = 0)
	mse = np.mean(beta_error**2, axis = 0)

	#beta_result = np.vstack((bias, squared_bias, variance, mse)).T
	#print beta_result

	# Calculate bias, squared bias, variance and MSE for prediction
	prediction_bias = np.mean(prediction_error)
	squared_pred_bias = prediction_bias ** 2
	prediction_var = np.var(prediction_error)
	prediction_MSE = np.mean(prediction_error**2)

	#pred_result = np.vstack((prediction_bias, squared_pred_bias, prediction_var, prediction_MSE)).T

	# Calculate bias, squared bias, variance and MSE for Y_hat
	f_bias = np.mean(f_error)
	squared_f_bias = f_bias ** 2
	f_var = np.var(f_error)
	f_MSE = np.mean(f_error**2)

	#f_result = np.vstack((f_bias, squared_f_bias, f_var, f_MSE)).T

	#total_result = np.vstack((beta_result, pred_result, f_result))
	#print total_result.shape
	#np.set_printoptions(precision=1, suppress=True)

	print ("#####################################")
	print ("Round %d, Number of observations: %d" % (j, n))
	print ("#####################################")

	print ("        bias  bias2  var  MSE")
	for i in range(p+1):
		print ("beta%d  %4.1f  %4.1f  %4.1f  %4.1f" % (i, bias[i], squared_bias[i], variance[i], mse[i]))
	print ("pred    %4.1f  %4.1f  %4.1f  %4.1f" % (prediction_bias, squared_pred_bias, prediction_var, prediction_MSE))
	print ("f       %4.1f  %4.1f  %4.1f  %4.1f" % (f_bias, squared_f_bias, f_var, f_MSE))

	j +=1

np.set_printoptions(precision=1, suppress=True)
print ("################")
print ("Betahat variance")
print ("################")

print (var_betahat)

